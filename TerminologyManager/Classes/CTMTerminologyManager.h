//
//  CTMTerminologyManager.h
//  Terminology Manager
//
//  Created by Anum Mian on 06/01/2015.
//  Copyright (c) 2015 Anum Mian. All rights reserved.
//

#import <Foundation/Foundation.h>

#define CTMTerminologyString(key, comment) \
[CTMTerminologyManager terminologyStringForKey:(key) alternate:(comment)]

@interface CTMTerminologyManager : NSObject

//Updates current localization file
+(void)setActiveCode:(NSString*)code;
+(void)updateTerminologyFileWithDictionary:(NSDictionary*)dictionary;
+(void)updateTerminologyFileWithDictionary:(NSDictionary*)dictionary withCode:(NSString*)code;
+(void)resetTerminology;
+(void)resetTerminologyForCode:(NSString*)code;
+(NSString*)terminologyStringForKey:(NSString*)key alternate:(NSString*)alternate;

@end
