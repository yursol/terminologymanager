//
//  CTMTerminologyManager.m
//  Terminology Manager
//
//  Created by Anum Mian on 06/01/2015.
//  Copyright (c) 2015 Anum Mian. All rights reserved.
//

#import "CTMTerminologyManager.h"

static NSDictionary *_terminologyDictionary;
static NSString *_activeCode;
NSString *const CMCTerminologyFolder = @"CMCTerminology";

@implementation CTMTerminologyManager

+(void)setActiveCode:(NSString*)code {
    _activeCode = code;
    [self setTerminologyCode:_activeCode];
    _terminologyDictionary = nil;
}

+(NSString*)activeCode {
    if (!_activeCode) {
        _activeCode = [self terminologyCode];
    }
    return _activeCode;
}

+(NSDictionary*)terminologyDictionary {
    if (!_terminologyDictionary) {
        if (self.activeCode){
            NSString *filePath = [self terminologyFilePathForCode: self.activeCode];
            _terminologyDictionary = [NSMutableDictionary dictionaryWithContentsOfFile:filePath];
        } else {
            _terminologyDictionary = [[self terminologyDictionary] mutableCopy];//we need to have mutable copy. User defaults always returns immutable copy. It crash appllication on iPad, but for some reason it isn't doing it on the simulator.
        }
    }
    return _terminologyDictionary;
}

+(NSDictionary*)terminologyDictionaryForCode:(NSString*)code {
    if (code){
        NSString *filePath = [self terminologyFilePathForCode: self.activeCode];
        return [NSMutableDictionary dictionaryWithContentsOfFile:filePath];
    }
    return self.terminologyDictionary;
}

+(void)resetTerminology {
    [self setStoredTerminologyDictionary:nil];
    
    NSString *terminologyPath = [self terminologyPath];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:terminologyPath]) {
        NSError *error;
        [fileManager removeItemAtPath:terminologyPath error:&error];
    }
}

+ (void)resetTerminologyForCode:(NSString*)code {
    NSString *filePath = [self terminologyFilePathForCode:code];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:filePath]) {
        NSError *error;
        [fileManager removeItemAtPath:filePath error:&error];
    }
}

+(void)updateTerminologyFileWithDictionary:(NSDictionary*)dictionary {
    [self updateTerminologyFileWithDictionary:dictionary withCode:nil];
}

+(void)updateTerminologyFileWithDictionary:(NSDictionary*)dictionary withCode:(NSString*)code {
    NSDictionary *currentDictionary = [self terminologyDictionaryForCode:code];
    if (code) {
        if (currentDictionary) {
            [currentDictionary setValuesForKeysWithDictionary:dictionary];
        } else {
            currentDictionary = dictionary;
        }
        
        [currentDictionary writeToFile:[self terminologyFilePathForCode:code] atomically:YES];
        if ([code isEqualToString:self.activeCode]) {
            _terminologyDictionary = nil;
        }
    } else {
        if (currentDictionary) {
            [_terminologyDictionary setValuesForKeysWithDictionary:dictionary];
        } else {
            _terminologyDictionary = dictionary;
        }
        [self setStoredTerminologyDictionary:_terminologyDictionary];
    }
}

+(NSString*)terminologyStringForKey:(NSString*)key alternate:(NSString*)alternate {
    NSDictionary *dictionary = [self terminologyDictionary];
    if (dictionary) {
        NSString *value = [dictionary valueForKey:key];
        if (value) {
            return value;
        }
    }
    return alternate;
}

+(NSString*)terminologyPath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *terminologyPath = [documentsDirectory stringByAppendingPathComponent:CMCTerminologyFolder];
    return terminologyPath;
}

+(NSString*)terminologyFilePathForCode:(NSString*)code {
    NSError *error;
    NSString *terminologyPath = [self terminologyPath];
    if (![[NSFileManager defaultManager] fileExistsAtPath:terminologyPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:terminologyPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
    }
    
    NSString *terminologyFilePath = [terminologyPath stringByAppendingFormat:@"%@.plist", code];
    return terminologyFilePath;
}

+(NSString*)terminologyCode {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"terminologyCode"];
}

+(void)setTerminologyCode:(NSString*)terminologyCode {
    [[NSUserDefaults standardUserDefaults] setValue:terminologyCode forKey:@"terminologyCode"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(NSDictionary*)storedTerminologyDictionary {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"CMCTerminologyDictionary"];
}

+(void)setStoredTerminologyDictionary:(NSDictionary*)dictionary {
    [[NSUserDefaults standardUserDefaults] setValue:dictionary forKey:@"CMCTerminologyDictionary"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
