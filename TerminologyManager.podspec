#
# Be sure to run `pod lib lint TerminologyManager.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'TerminologyManager'
  s.version          = '0.1.1'
  s.summary          = 'TerminologyManager is used for COINS terminology.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TerminologyManager is used to display tranclated COINS terminology.
                       DESC

  s.homepage         = 'https://bitbucket.org/yursol/terminologymanager'
  s.license          = { :type => 'MIT', :text => 'LICENSE' }
  s.author           = { 'Yuri Solodkin' => 'yuri.solodkin@coins-global.com' }
  s.source           = { :git => 'https://bitbucket.org/yursol/terminologymanager.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = 'TerminologyManager/Classes/**/*'
  
end
