//
//  CTMAppDelegate.h
//  TerminologyManager
//
//  Created by Yuri Solodkin on 07/20/2016.
//  Copyright (c) 2016 Yuri Solodkin. All rights reserved.
//

@import UIKit;

@interface CTMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
