//
//  main.m
//  TerminologyManager
//
//  Created by Yuri Solodkin on 07/20/2016.
//  Copyright (c) 2016 Yuri Solodkin. All rights reserved.
//

@import UIKit;
#import "CTMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CTMAppDelegate class]));
    }
}
